<?php

/**
 * @brief       Build Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Base
 * @since       1.2.0
 * @version     -storm_version-
 */

namespace IPS\dtbase;
use function implode;

use Exception;
use IPS\Application;
use IPS\Application\BuilderIterator;
use IPS\dtbase\Profiler\Debug;
use IPS\Http\Url;
use IPS\Output;
use IPS\Patterns\Singleton;
use IPS\Request;
use Phar;
use PharData;
use RuntimeException;
use \Slasher\Slasher;
use ZipArchive;
use function chmod;
use function copy;
use function file_exists;
use function is_dir;
use function mkdir;
use function sprintf;

class _Build extends Singleton
{
    protected static $instance;

    /**
     * @param $app
     * @throws \InvalidArgumentException
     * @throws \OutOfRangeException
     * @throws Exception
     */
    public function buildAndExport($app = \null, $slasher = \false, array $skip = [])
    {
        $app = $app ?? Request::i()->app;
        $application = Application::load($app);
        Request::i()->appKey = $app;
        $e = [];
        $e[] = [
            'name' => 'long_version',
            'class' => '#',
            'label' => 'Long Version',
            'required' => \true,
            'default' => $application->long_version,
        ];
        $e[] = [
            'name' => 'short_version',
            'label' => 'Short Version',
            'required' => \true,
            'default' => $application->version,
        ];

        $form = Forms::execute(['elements' => $e]);

        if ($values = $form->values()) {
            $long = $values[ 'long_version' ];
            $short = $values[ 'short_version' ];
            $appName = $application->directory;
            $name = $appName . '-' . $short;
            $path = \IPS\ROOT_PATH . '/' . $appName . '/' . $short . '/';
            $defaults = \IPS\ROOT_PATH . '/applications/' . $appName . '/data/defaults/';
            $pharPath = \null;

            if ($slasher) {
                require_once \IPS\ROOT_PATH . '/applications/slasher/slasher.php';

                //lets slash them before we go forward
                $appPath = \IPS\ROOT_PATH . '/applications/' . $application->directory . '/';
                $args = [
                    'foo.php',
                    $appPath,
                    '-all',
                    '-use',
                ];

                if (!empty($skip)) {
                    $args[] = '-skip='.implode(',', $skip);
                }

                (new Slasher($args, \true))->execute();
            }


            try {
                $application->build();
                $application->assignNewVersion($long, $short);

                if (!is_dir($path)) {
                    if (!mkdir($path, \IPS\IPS_FOLDER_PERMISSION, \true) && !is_dir($path)) {
                        throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
                    }
                    chmod($path, \IPS\IPS_FOLDER_PERMISSION);
                }

                $pharPath = $path . $appName . '.tar';
                $download = new PharData($pharPath, 0, $appName . '.tar', Phar::TAR);
                $download->buildFromIterator(new BuilderIterator($application));
            } catch (Exception $e) {
                Debug::add('phar', $e, \true);
            }

            $files = [];

            $files[] = $appName . '.tar';

            if (file_exists($defaults . 'directions.txt')) {
                $files[] = 'directions.txt';
                copy($defaults . 'directions.txt', $path . 'directions.txt');
            }

            $zip = new ZipArchive;

            if ($zip->open($path . $name . '.zip', ZIPARCHIVE::CREATE) === \true) {
                foreach ($files as $file) {
                    $zip->addFile($path . $file, $file);
                    if ($file !== $appName . '.tar') {
                        unset($file);
                    }
                }
                $zip->close();
            }

            unset($download);
            \Phar::unlinkArchive($pharPath);

            $url = Url::internal('app=core');
            Output::i()->redirect($url, $app . ' Built');
        }

        Output::i()->title = 'Build ' . $app;
        Output::i()->output = $form;
    }
}
