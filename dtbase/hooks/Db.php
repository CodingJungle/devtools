//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */

use IPS\dtprofiler\Profiler\Memory;
use IPS\dtproxy\Generator\Db;
use IPS\dtproxy\Generator\Proxy;
use IPS\dtproxy\Proxyclass;

if ( !\defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class dtbase_hook_Db extends _HOOK_CLASS_
{
    protected $dtkey = 0;

    /**
     * @inheritdoc
     */
    public function query($query, $log = \TRUE, $read = \FALSE)
    {
        if (\IPS\QUERY_LOG && \class_exists(Memory::class, \true)) {
            Memory::start('query');
            $start = \microtime(\true);
        }
        $parent = parent::query($query, $log, $read);

        if (\IPS\QUERY_LOG  && \class_exists(Memory::class, \true)) {
            $final = \microtime(\true) - $start;
            $mem = Memory::end(\false);
            $this->finalizeLog($final, $mem);
        }

        return $parent;
    }

    /**
     * @param $time
     * @param $mem
     */
    protected function finalizeLog($time, $mem)
    {
        $id = $this->dtkey - 1;
        $this->log[ $id ][ 'time' ] = $time;
        $this->log[ $id ][ 'mem' ] = $mem;
    }

    /**
     * @inheritdoc
     * @throws \IPS\Db\Exception
     */
    public function preparedQuery($query, array $_binds, $read = \FALSE)
    {
        if (\IPS\QUERY_LOG  && \class_exists(Memory::class, \true)) {
            Memory::start('query');
            $start = \microtime(\true);
        }

        $parent = parent::preparedQuery($query, $_binds, $read);

        if (\IPS\QUERY_LOG && \class_exists(Memory::class, \true)) {
            $final = \microtime(\true) - $start;
            $mem = Memory::end(\false);
            $this->finalizeLog($final, $mem);
        }

        return $parent;
    }

    /**
     * @inheritdoc
     */
    protected function log($query, $server = \null)
    {
        $this->dtkey++;
        parent::log($query, $server);
    }

    /**
     * @inheritdoc
     */
    public function createTable($data)
    {
        $return = parent::createTable($data);

        if( \class_exists(Proxyclass::class, \true) ){
            Db::i()->create();
        }
        return $return;
    }

    /**
     * @inheritdoc
     */
    public function addColumn($table, $definition)
    {
        parent::addColumn($table, $definition);
        if( \class_exists( Proxy::class, \true) ){
            Proxy::adjustModel($table);
        }
    }
}
