//<?php


use IPS\dtdevfolder\Apps;
use IPS\dtdevplus\Headerdoc;
use IPS\Task;

if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtbase_hook_Application extends _HOOK_CLASS_
{
    /**
     * @inheritdoc
     * @throws \InvalidArgumentException
     * @throws UnderflowException
     * @throws RuntimeException
     */
    public function assignNewVersion($long, $human)
    {
        parent::assignNewVersion($long, $human);
        if (static::appIsEnabled('dtdevplus')) {
            $this->version = $human;
            Headerdoc::i()->process($this);
        }
    }

    /**
     * @inheritdoc
     */
    public function build()
    {
        if (static::appIsEnabled('dtdevplus')) {
            Headerdoc::i()->addIndexHtml($this);
        }
        parent::build();
    }

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function installOther()
    {
        if (static::appIsEnabled('dtdevfolder')) {
            if (\IPS\IN_DEV) {
                $dir = \IPS\ROOT_PATH . '/applications/' . $this->directory . '/dev/';
                if (!\file_exists($dir)) {
                    $app = new Apps($this);
                    $app->addToStack = \true;
                    $app->email();
                    $app->javascript();
                    $app->language();
                    $app->templates();
                }
            }
        }

        parent::installOther();

        if (static::appIsEnabled('dtproxy')) {
//            Task::queue('dtproxy', 'dtProxy', [], 5, ['dtProxy']);
        }
    }

}
