//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */

use IPS\Output;
use IPS\Theme;

if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtbase_hook_adminGlobal extends _HOOK_CLASS_
{

    /* !Hook Data - DO NOT REMOVE */
    public static function hookData()
    {
        return \array_merge_recursive([
            'globalTemplate' => [
                0 => [
                    'selector' => '#ipsLayout_header',
                    'type' => 'add_inside_start',
                    'content' => '{{if $menu = \IPS\dtbase\Menu::i()->build()}}
	{$menu|raw}
{{endif}}',
                ],
            ],
        ], parent::hookData());
    }

    /* End Hook Data */

    public function globalTemplate($title, $html, $location = [])
    {
        Output::i()->cssFiles = \array_merge(Output::i()->cssFiles, Theme::i()->css('devbar.css', 'dtbase', 'admin'));

        return parent::globalTemplate($title, $html, $location);
    }

}
