<?php

$lang = [
    '__app_dtbase' => 'Dev Toolbox: Base',
    'menutab__devtools' => 'Dev Toolbox',
    'menutab__devtools_icon' => 'wrench',
    'dtbase_apps_built' => 'Apps built',
    'menu__dtbase_settings' => 'Dev Toolbox',
    'menu__dtbase_settings_settings' => 'Settings',
    'ext__menu' => 'Add menus to the DevBar',
    'ext__settings' => 'Settings for the other apps to add to the settings in dt base (so we aren\'t using if appIsenable checks, which makes the code ugly)',
    'ext__Headerdoc' => 'adds the "Headerdoc" extension to your app to gain features such as class commenting, adding blank index.html\'s to all the folders in your app or skiping files or entire directories in the build process. the name is misleading :P',
    'ext__SourcesForm' => 'this is experimental atm, nothing to see here.',
    'ext__ProxyHelpers' => 'add "properties" to the Data\\Store or Request class thru here.',
];
