<?php

/**
 * @brief       Settings Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Base
 * @since       1.1.0
 * @version     -storm_version-
 */

namespace IPS\dtbase\modules\admin\settings;
use function preg_replace_callback;

use Exception;
use IPS\Application;
use IPS\Dispatcher;
use IPS\Dispatcher\Controller;
use IPS\dtbase\Forms;
use IPS\Helpers\Form;
use IPS\Output;
use IPS\Request;
use IPS\Settings;
use function defined;
use function header;

/* To prevent PHP errors (extending class does not exist) revealing path */

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * settings
 */
class _settings extends Controller
{
    /**
     * Execute
     *
     * @return    void
     * @throws \RuntimeException
     */
    public function execute()
    {
        Dispatcher\Admin::i()->checkAcpPermission('settings_manage');
        parent::execute();
    }

    /**
     * ...
     *
     * @return    void
     */
    protected function manage()
    {
        Output::i()->sidebar[ 'actions' ][ 'init' ] = [
            'icon' => 'plus',
            'title' => 'Patch init.php',
            'link' => Request::i()->url()->setQueryString(['do' => 'patchInit']),

        ];

        $elements = [];
        /* @var \IPS\dtbase\extensions\dtbase\Settings\settings $extension */
        foreach (Application::allExtensions('dtbase', 'settings') as $extension) {
            $elements[] = [
                'type' => 'tab',
                'name' => $extension->tab(),
            ];
            $extension->elements($elements);
        }

        $config = [
            'elements' => $elements,
            'object' => Settings::i(),
        ];

        /**
         * @var Form $form
         */
        try {
            $form = Forms::execute($config);

            if ($values = $form->values()) {

                foreach (Application::appsWithExtension('dtbase', 'settings') as $app) {
                    $extensions = $app->extensions('dtbase', 'settings', \true);
                    /* @var \IPS\dtbase\extensions\dtbase\settings\settings $extension */
                    foreach ($extensions as $extension) {
                        $extension->formateValues($values);
                    }
                }

                $form->saveAsSettings($values);
                Output::i()->redirect($this->url->setQueryString(['tab' => '']));
            }

            Output::i()->title = 'Settings';
            Output::i()->output = $form;
        } catch (Exception $e) {
        }

    }

    protected function patchInit()
    {
        $path = \IPS\ROOT_PATH . \DIRECTORY_SEPARATOR;
        $init = $path . 'init.php';
        $content = \file_get_contents($init);
        $preg = "#public static function monkeyPatch\((.*?)public#msu";
        $before = <<<'eof'
public static function monkeyPatch($namespace, $finalClass, $extraCode = '')
    {
        $realClass = "_{$finalClass}";

        if (isset(self::$hooks[ "\\{$namespace}\\{$finalClass}" ]) AND \IPS\RECOVERY_MODE === false) {
            $path = ROOT_PATH . '/hook_temp/';
            if (!\is_dir($path)) {
                \mkdir($path, 0777, true);
            }

            foreach (self::$hooks[ "\\{$namespace}\\{$finalClass}" ] as $id => $data) {
                if (\file_exists(ROOT_PATH . '/' . $data[ 'file' ])) {
                    $contents = "namespace {$namespace}; " . str_replace('_HOOK_CLASS_', $realClass,
                            file_get_contents(ROOT_PATH . '/' . $data[ 'file' ]));
                    $hash = md5($contents);
                    $filename = \str_replace(["\\", '/'], '_', $namespace . $realClass . $finalClass . $data[ 'file' ]);

                    $filename .= '_' . $hash . '.php';
                    if (!\file_exists($path . $filename)) {
                        \file_put_contents($path . $filename, "<?php\n\n" . $contents);
                    }
                    require_once $path . $filename;
                    $realClass = $data[ 'class' ];
                }
            }
        }
        $reflection = new \ReflectionClass("{$namespace}\\_{$finalClass}");
        if (eval("namespace {$namespace}; " . $extraCode . ($reflection->isAbstract() ? 'abstract' : '') . " class {$finalClass} extends {$realClass} {}") === false) {
            trigger_error("There was an error initiating the class {$namespace}\\{$finalClass}.", E_USER_ERROR);
        }
    }
eof;

        $content = preg_replace_callback($preg, function ($e) use ($before) {
            return $before . "\n\n  public";
        }, $content);

        \file_put_contents($init, $content);
        Output::i()->redirect($this->url, 'init.php patched');

    }
}
