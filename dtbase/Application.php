<?php
/**
 * @brief            Dev Toolbox: Base Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Base
 * @since            02 Apr 2018
 * @version          -storm_version-
 */

namespace IPS\dtbase;
use function file_exists;

/**
 * Dev Toolbox: Base Application Class
 */
class _Application extends \IPS\Application
{
    public static $toolBoxApps = [
        'dtbase',
        'dtcode',
        'dtdevfolder',
        'dtdevplus',
        'dtproxy',
    ];
    /**
     * @var string
     */
    protected static $baseDir = \IPS\ROOT_PATH . '/applications/dtbase/sources/vendor/';

    protected static $loaded = \false;

    public static function loadAutoLoader()
    {
        if (static::$loaded === \false) {
            static::$loaded = \true;
            $available = [
                'symfony/pollyfill-mbstring' => [
                    'name' => 'Symfony Polyfill-Mbstring',
                    'version' => '1.8',
                    'namespace' => 'Symfony',
                    'path' => 'Symfony',
                    'fullpath' => 'Polyfill/Mbstring',
                    'type' => 'php',
                    'files' => ['bootstrap.php'],
                ],
                'symfony/finder' => [
                    'name' => 'Symfony Finder',
                    'version' => '3.4',
                    'namespace' => 'Symfony',
                    'path' => 'Symfony',
                    'fullpath' => 'Component/Finder',
                    'type' => 'php',
                ],
                'symfony/filesystem' => [
                    'name' => 'Symfony Filesystem',
                    'version' => '3.4',
                    'namespace' => 'Symfony',
                    'path' => 'Symfony',
                    'fullpath' => 'Component/Filesystem',
                    'type' => 'php',
                ],
                'symfony/var-dumper' => [
                    'name' => 'Symfony VarDumper',
                    'version' => '3.4',
                    'path' => 'Symfony',
                    'fullpath' => 'Component/VarDumper',
                    'namespace' => 'Symfony',
                    'type' => 'php',
                    'files' => ['Resources/functions/dump.php'],
                ],
                'zend/code' => [
                    'name' => 'Zend Code',
                    'version' => '3.3',
                    'path' => 'Zend',
                    'fullpath' => 'Code',
                    'namespace' => 'Zend',
                    'type' => 'php',
                ],
                'zend/eventmanager' => [
                    'name' => 'Zend EventManager',
                    'version' => '3.3',
                    'path' => 'Zend',
                    'fullpath' => 'EventManager',
                    'namespace' => 'Zend',
                    'type' => 'php',
                ],
//            'erusev/parsedown' => [
//                'name' => 'Erusev Parsedown',
//                'version' => '1.7',
//                'path' => 'Erusev',
//                'fullpath' => 'Parsedown',
//                'namespace' => 'Erusev',
//                'type' => 'php',
//                'download' => 'erusev-parsedown'
//            ]
            ];
            foreach ($available as $lib) {
                static::buildForAutoload($lib);
            }
        }
    }

    /**
     * @param array $lib
     */
    protected static function buildForAutoload(array $lib)
    {
        $path = static::$baseDir . $lib['path'] . '/';
        if (!isset(\IPS\IPS::$PSR0Namespaces[$lib['namespace']])) {
            \IPS\IPS::$PSR0Namespaces[$lib['namespace']] = $path;
        }

        if (!empty($lib['files'])) {
            $files = $lib['files'];
            foreach ($files as $file) {
                $location = $path . $lib['fullpath'] . '/' . $file;
                if (file_exists($location)) {
                    @require $location;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
