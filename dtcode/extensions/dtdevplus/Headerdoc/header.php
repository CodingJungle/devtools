<?php

/**
 * @brief       Dtdevplus Headerdoc extension: Header
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Code Analyzer
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtcode\extensions\dtdevplus\Headerdoc;

use IPS\dtdevplus\Headerdoc\HeaderdocAbstract;
use function defined;
use function header;

/* To prevent PHP errors (extending class does not exist) revealing path */

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * header
 */
class _header extends HeaderdocAbstract
{

    /**
     * enable headerdoc
     **/
    public function enabled(): bool
    {
        return \true;
    }

    /**
     * if enabled, will add a blank index.html to each folder
     **/
    public function indexEnabled(): bool
    {
        return \true;
    }

    /**
     * files to skip during building of the tar
     * @param $skip
     */
    public function filesSkip(&$skip)
    {

    }

    /**
     * directories to skip during building of the tar
     * @param $skip
     */
    public function dirSkip(&$skip)
    {

    }

    /**
     * an array of files/folders to exclude in the headerdoc
     * @param $skip
     */
    public function exclude(&$skip)
    {

    }
}
