<?php
/**
 * @brief            Dev Toolbox: Code Analyzer Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Code Analyzer
 * @since            28 Mar 2018
 * @version          -storm_version-
 */

namespace IPS\dtcode;

/**
 * Dev Toolbox: Code Analyzer Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
