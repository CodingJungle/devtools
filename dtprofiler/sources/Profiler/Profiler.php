<?php

/**
 * @brief       Profile Singleton
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler;

use Exception;
use IPS\Application;
use IPS\Dispatcher;
use IPS\dtbase\Editor;
use IPS\dtprofiler\Parsers\Caching;
use IPS\dtprofiler\Parsers\Database;
use IPS\dtprofiler\Parsers\Files;
use IPS\dtprofiler\Parsers\Logs;
use IPS\dtprofiler\Parsers\Templates;
use IPS\dtprofiler\Profiler\Debug;
use IPS\dtprofiler\Profiler\Memory;
use IPS\Member;
use IPS\Patterns\Singleton;
use IPS\Request;
use IPS\Settings;
use IPS\Theme;
use ReflectionClass;
use function count;
use function defined;
use function header;
use function implode;
use function is_array;
use function microtime;
use function round;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

class _Profiler extends Singleton
{

    /**
     * @brief   Singleton Instances
     * @note    This needs to be declared in any child class.
     */
    protected static $instance;


    /**
     * @return mixed
     * @throws \InvalidArgumentException
     * @throws \OutOfRangeException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    public function run()
    {
        if (!Application::appIsEnabled('dtbase') || ( !Member::loggedIn()->member_id && \IPS\CACHE_PAGE_TIMEOUT !== 0 ) ) {
            return '';
        }
        if (!Request::i()->isAjax()) {
            $framework = \null;

            if (Settings::i()->dtprofiler_enabled_execution) {
                $framework = round(microtime(\true) - $_SERVER[ 'REQUEST_TIME_FLOAT' ], 4) * 1000;
            }
//
            $logs = Logs::i()->build();
            $database = Database::i()->build();
            $templates = Templates::i()->build();
            $extra = implode(' ', $this->extra());
            $info = $this->info();
            $environment = $this->environment();
            $debug = Debug::build();
            $files = \null;
            $memory = \null;
            $cache = \null;
            $time = \null;

            if (Settings::i()->dtprofiler_enabled_files) {
                $files = Files::i()->build();
            }

            if (\IPS\CACHING_LOG) {
                $cache = Caching::i()->build();
            }

            if (Settings::i()->dtprofiler_enabled_memory) {
                $memory = Memory::build();
            }

            if (Settings::i()->dtprofiler_enabled_execution) {
                $total = round(microtime(\true) - $_SERVER[ 'REQUEST_TIME_FLOAT' ], 4) * 1000;
                $profileTime = $total - $framework;
                $time = [
                    'total' => $total,
                    'framework' => $framework,
                    'profiler' => $profileTime,
                ];
            }

            return Theme::i()
                        ->getTemplate('bar', 'dtprofiler', 'front')
                        ->bar($time, $memory, $files, $templates, $database, $cache, $logs, $extra, $info, $environment, $debug);
        }

        return \null;
    }

    /**
     * hook into this to add "buttons" to the bar
     * @return array
     */
    protected function extra(): array
    {
        return [];
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     * @throws \OutOfRangeException
     * @throws \RuntimeException
     */
    protected function info(): array
    {
        $info = [];
        $info[ 'IPS' ] = Application::load('core')->version;
        $info[ 'php' ] = \PHP_VERSION;
        $info[ 'Controller' ] = $this->getLocation();
        return $info;
    }

    /**
     * @return array|string
     * @throws \RuntimeException
     */
    protected function getLocation()
    {
        $location = [];
        $url = \IPS\ROOT_PATH . '/applications/';
        if (isset(Request::i()->app)) {
            $url .= Request::i()->app . '/';
            $location[] = Request::i()->app;
        }

        if (isset(Request::i()->module)) {
            $url .= 'modules/';
            $location[] = 'modules';
            if (Dispatcher::hasInstance()) {
                if (Dispatcher::i() instanceof Dispatcher\Front) {
                    $url .= 'front/';
                    $location[] = 'front';
                } else {
                    $url .= 'admin/';
                    $location[] = 'admin';
                }
            }
            $url .= Request::i()->module . '/';
            $location[] = Request::i()->module;
        }

        if (isset(Request::i()->controller)) {
            $url .= Request::i()->controller . '.php';
            $location[] = Request::i()->controller;
        }

        if (isset(Request::i()->do)) {
            $do = Request::i()->do;
        } else {
            $do = 'manage';
        }

        $link = (new Editor)->replace($url);
        $class = 'IPS\\' . implode('\\', $location);
        $location = $class . '::' . $do;

        try {
            $reflection = new ReflectionClass($class);
            $line = $reflection->getMethod($do)->getStartLine();
            $location .= ':' . $line;
        } catch (Exception $e) {
            $line = \null;
        }

        if ($link) {
            $url = (new Editor)->replace($url, $line);
            return '<a href="' . $url . '">' . $location . '</a>';
        }

        return $location;
    }

    /**
     * @return null|array
     * @throws \UnexpectedValueException
     */
    protected function environment()
    {
        if (!Settings::i()->dtprofiler_enabled_enivro) {
            return \null;
        }

        $data = [];
        $count = 0;

        if (!empty($_GET)) {
            $count += count($_GET);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($_GET, '$_GET Data');
        }

        if (!empty($_POST)) {
            $count += count($_POST);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($_POST, '$_POST Data');
        }

        if (!empty(Request::i()->returnData())) {
            $request = Request::i()->returnData();
            $count += count($request);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($request, '$_REQUEST Data');
        }

        if (!empty($_COOKIE)) {
            $count += count($_COOKIE);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($_COOKIE, '$_COOKIE Data');
        }

        if (!empty($_SESSION)) {
            $count += count($_SESSION);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($_SESSION, '$_SESSION Data');
        }

        if ( !empty($_SERVER)) {
            $count += count($_SERVER);
            $data[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->keyvalue($_SERVER, '$_SERVER Data');
        }

        $return = \null;
        if (is_array($data) && count($data)) {
            $return = Theme::i()
                           ->getTemplate('generic', 'dtprofiler', 'front')
                           ->button('Environment', 'environment', 'Environment Variables', $data, $count);

        }

        return $return;
    }
}
