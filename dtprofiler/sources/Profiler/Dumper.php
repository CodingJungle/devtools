<?php

/**
 * @brief       Dumper Singleton
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\Profiler;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use function defined;
use function header;


if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

\IPS\dtbase\Application::loadAutoLoader();

class _Dumper extends HtmlDumper
{
    public function add($value)
    {
        $cloner = new VarCloner;
        return $this->dump($cloner->cloneVar($value), \true);
    }
}
