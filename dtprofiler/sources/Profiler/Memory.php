<?php

/**
 * @brief       Memory Active Record
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\Profiler;

use IPS\Db;
use IPS\Patterns\ActiveRecord;
use IPS\Patterns\ActiveRecordIterator;
use IPS\Request;
use IPS\Theme;
use function count;
use function defined;
use function floor;
use function header;
use function log;
use function memory_get_usage;
use function round;
use function time;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * Class _Memory
 * @package IPS\dtprofiler\Profiler
 * @mixin \IPS\dtprofiler\Profiler\Memory
 */
class _Memory extends ActiveRecord
{
    /**
     * @brief    [ActiveRecord] Database Prefix
     */
    public static $databasePrefix = 'memory_';

    /**
     * @brief    [ActiveRecord] Database table
     */
    public static $databaseTable = 'dtprofiler_memory';

    /**
     * @brief   Bitwise keys
     */
    public static $bitOptions = [
        'bitoptions' => [
            'bitoptions' => [],
        ],
    ];

    /**
     * @brief    [ActiveRecord] Multiton Store
     */
    protected static $multitons = [];

    /**
     * start time
     * @var null
     */
    protected static $start = [];

    protected static $keyed;

    public static function start($key = \null)
    {
        static::$keyed = $key;
        static::$start[ $key ?: 0 ] = memory_get_usage();
    }

    public static function end($log = \true)
    {
        $end = memory_get_usage();
        $mem = $end - static::$start[ static::$keyed ?: 0 ];
        $mem = static::formatBytes($mem);
        if (static::$keyed !== \null && $log) {
            $key = static::$keyed;
            $memory = new static;
            $memory->key = $key;
            $memory->log = $mem;
            $memory->time = time();
            if (Request::i()->isAjax()) {
                $memory->ajax = 1;
            }
            $memory->save();
        }

        return $mem;
    }

    /**
     * @param     $size
     * @param int $precision
     * @return string
     */
    public static function formatBytes($size, $precision = 2): string
    {
        $base = log($size, 1024);
        $suffixes = ['B', 'KB', 'MB', 'GB', 'TB'];
        $expo = 1024 ** ($base - floor($base));
        $suffix = (int)floor($base);
        return round($expo, $precision) . ' ' . $suffixes[ $suffix ];
    }

    /**
     * @throws \UnexpectedValueException
     */
    public static function build()
    {
        $list = [];
        $sql = Db::i()->select('*', 'dtprofiler_memory', ['memory_ajax = ?', 0], 'memory_id DESC');
        $iterators = new ActiveRecordIterator($sql, Memory::class);
        /* @var Memory $obj */
        foreach ($iterators as $obj) {
            $data = $obj->key . ' : ' . $obj->log;
            $list[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->plain($data);
            $obj->delete();
        }

        $count = count($list) ?: \null;
        $total = static::total();

        return Theme::i()
                    ->getTemplate('generic', 'dtprofiler', 'front')
                    ->button($total, 'memory', 'Memory Total', $list, $count, 'microchip', \false);
    }

    /**
     * @return string
     */
    protected static function total(): string
    {
        return static::formatBytes(memory_get_usage());
    }
}
