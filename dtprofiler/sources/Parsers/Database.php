<?php

/**
 * @brief       Database Standard
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\Parsers;

use IPS\Data\Store;
use IPS\Db;
use IPS\Http\Url;
use IPS\Patterns\Singleton;
use IPS\Theme;
use function count;
use function defined;
use function header;
use function round;
use function sha1;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

class _Database extends Singleton
{
    /**
     * @brief   Singleton Instances
     * @note    This needs to be declared in any child class.
     */
    protected static $instance;

    /**
     * query store
     * @var array
     */
    protected $dbQueries = [];

    /**
     * _Database constructor.
     */
    public function __construct()
    {
        $this->dbQueries = Db::i()->log;
    }

    /**
     * builds the database button
     * @return mixed
     * @throws \UnexpectedValueException
     */
    public function build()
    {
        $list = [];
        $hash = [];
        $dbs = $this->dbQueries;

        foreach ($dbs as $db) {
            $h = sha1($db[ 'query' ]);
            $hash[ $h ] = ['query' => $db[ 'query' ], 'bt' => $db[ 'backtrace' ]];
            $url = Url::internal('app=dtprofiler&module=bt&controller=bt', 'front')->setQueryString(['bt' => $h]);
            $time = \null;
            if (isset($db[ 'time' ])) {
                $time = round($db[ 'time' ], 4);
            }

            $mem = \null;
            if (isset($db[ 'mem' ])) {
                $mem = $db[ 'mem' ];
            }

            $list[] = [
                'server' => $db[ 'server' ] ?? \null,
                'query' => $db[ 'query' ],
                'url' => $url,
                'time' => $time,
                'mem' => $mem,
            ];
        }
        Store::i()->dtprofiler_bt = $hash;

        return Theme::i()->getTemplate('database', 'dtprofiler', 'front')->database($list, count($list));
    }
}
