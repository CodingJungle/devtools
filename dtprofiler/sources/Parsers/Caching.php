<?php

/**
 * @brief       Caching Standard
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\Parsers;

use IPS\Data\Cache;
use IPS\Data\Store;
use IPS\Http\Url;
use IPS\Patterns\Singleton;
use IPS\Theme;
use function count;
use function defined;
use function header;
use function sha1;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

class _Caching extends Singleton
{

    /**
     * @brief   Singleton Instances
     * @note    This needs to be declared in any child class.
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * _Caching constructor.
     */
    public function __construct()
    {
        $this->cache = Cache::i()->log + Store::i()->log;
    }

    /**
     * @throws \UnexpectedValueException
     */
    public function build(): string
    {

        $list = [];
        $hash = [];
        $caches = $this->cache;

        foreach ($caches as list($type, $name, $content, $bt)) {
            if ($type === 'check') {
                continue;
            }

            $h = sha1($type . $name);
            $hash[ $h ] = ['content' => $content, 'bt' => $bt];
            $url = Url::internal('app=dtprofiler&module=bt&controller=bt', 'front')->setQueryString([
                'do' => 'cache',
                'bt' => $h,
            ]);
            $list[ $h ] = ['type' => $type, 'name' => $name, 'url' => $url];
        }

        Store::i()->dtprofiler_bt_cache = $hash;

        return Theme::i()->getTemplate('caching', 'dtprofiler', 'front')->caching($list, count($list));
    }
}
