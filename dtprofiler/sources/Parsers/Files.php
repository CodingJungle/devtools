<?php

/**
 * @brief       Files Singleton
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtprofiler
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\Parsers;

use IPS\dtbase\Editor;
use IPS\Patterns\Singleton;
use IPS\Theme;
use function count;
use function defined;
use function get_included_files;
use function header;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

class _Files extends Singleton
{

    /**
     * @brief   Singleton Instances
     * @note    This needs to be declared in any child class.
     */
    protected static $instance;

    /**
     * builds the files button
     * @throws \UnexpectedValueException
     */
    public function build(): string
    {
        $files = get_included_files();
        $count = count($files);
        $list = [];
        foreach ($files as $key => $file) {
            $url = (new Editor)->replace($file);
            $list[] = Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->anchor($key . '. ' . $file, $url);
        }

        return Theme::i()
                    ->getTemplate('generic', 'dtprofiler', 'front')
                    ->button('Files', 'files', 'Total Include Files', $list, $count, 'file', \false);
    }

}
