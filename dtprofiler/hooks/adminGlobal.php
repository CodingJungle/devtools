//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtprofiler_hook_adminGlobal extends _HOOK_CLASS_
{

    /* !Hook Data - DO NOT REMOVE */
    public static function hookData()
    {
        return \array_merge_recursive([
            'globalTemplate' => [
                0 => [
                    'selector' => 'html > body',
                    'type' => 'add_inside_end',
                    'content' => '<!--ipsQueryLog-->',
                ],
            ],
        ], parent::hookData());
    }
    /* End Hook Data */


}
