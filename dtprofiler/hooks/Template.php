//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */

use IPS\Data\Store;
use IPS\Request;
use IPS\Settings;

if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtprofiler_hook_Template extends _HOOK_CLASS_
{
    public function __call($bit, $params)
    {

        if (!Request::i()
                    ->isAjax() && \IPS\QUERY_LOG && Settings::i()->dtprofiler_enabled_templates && $this->app !== 'dtprofiler' && ($this->app === 'core' && $bit !== 'cachingLog')) {
            if (isset(Store::i()->dtprofiler_templates)) {
                $log = Store::i()->dtprofiler_templates;
            }
            $log[] = [
                'name' => $bit,
                'group' => $this->templateName,
                'location' => $this->templateLocation,
                'app' => $this->app,
            ];

            Store::i()->dtprofiler_templates = $log;
        }
        return parent::__call($bit, $params);
    }
}
