<?php

/**
 * @brief       Bt Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Profiler
 * @since       1.0.3
 * @version     -storm_version-
 */

namespace IPS\dtprofiler\modules\front\bt;

use Exception;
use IPS\Data\Store;
use IPS\Db;
use IPS\Dispatcher\Controller;
use IPS\dtprofiler\Profiler\Debug;
use IPS\Log;
use IPS\Output;
use IPS\Patterns\ActiveRecordIterator;
use IPS\Request;
use IPS\Theme;
use function count;
use function defined;
use function header;
use function htmlentities;
use function ini_get;
use function is_array;
use function nl2br;
use function sleep;
use function str_replace;
use function time;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * bt
 */
class _bt extends Controller
{

    /**
     * @inheritdoc
     */
    protected function manage()
    {
        $store = Store::i()->dtprofiler_bt;
        $hash = Request::i()->bt;
        $output = 'Nothing Found';
        if (isset($store[ $hash ])) {
            $bt = str_replace("\\\\", "\\", $store[ $hash ][ 'bt' ]);
            $output = '<code>' . $store[ $hash ][ 'query' ] . '</code><br><pre class="prettyprint lang-php">' . $bt . '</pre>';

        }

        Output::i()->output = "<div class='ipsPad'>{$output}</div>";

    }

    /**
     * shows data for the cache dialog
     */
    protected function cache()
    {
        $store = Store::i()->dtprofiler_bt_cache;
        $hash = Request::i()->bt;
        $output = 'Nothing Found';
        if (isset($store[ $hash ])) {
            $bt = str_replace("\\\\", "\\", $store[ $hash ][ 'bt' ]);
            $content = nl2br(htmlentities($store[ $hash ][ 'content' ]));
            $output = '<code>' . $content . '</code><br><pre class="prettyprint lang-php">' . $bt . '</pre>';

        }

        Output::i()->output = "<div class='ipsPad'>{$output}</div>";

    }

    /**
     * shows data for the logs dialog
     */
    protected function log()
    {
        $id = Request::i()->id;
        $output = 'Nothing Found';
        try {
            $log = Log::load($id);
            $output = $log->category . '<br><code>' . $log->message . '</code><br><pre class="prettyprint lang-php">' . $log->backtrace . '</pre>';

        } catch (Exception $e) {
        }

        Output::i()->output = "<div class='ipsPad'>{$output}</div>";

    }

    /**
     * long polling for debug() messages
     * @throws \UnexpectedValueException
     */
    protected function debug()
    {
        $max = (ini_get('max_execution_time') / 2) - 5;
        $time = time();
        $since = Request::i()->last ?: 0;
        while (\true) {
            $ct = time() - $time;
            if ($ct >= $max) {
                Output::i()->json(['error' => 1]);
            }

            $query = Db::i()
                       ->select('*', 'dtprofiler_debug',
                           ['debug_ajax = ? AND debug_id > ? AND debug_viewed=?', 1, $since, 0], \null, \null, \null, \null,Db::SELECT_SQL_CALC_FOUND_ROWS);

            if ($query->count(\true)) {

                $iterators = new ActiveRecordIterator($query, Debug::class);

                $last = 0;

                /* @var \IPS\dtprofiler\Profiler\Debug $obj */
                foreach ($iterators as $obj) {
                    $list[] = $obj->body();
                    $last = $obj->id;
                }

                $return = [];
                if (is_array($list) && count($list)) {
                    $count = count($list);
                    $return[ 'count' ] = $count;
                    $lists = '';
                    foreach ($list as $l) {
                        $lists .= Theme::i()->getTemplate('generic', 'dtprofiler', 'front')->li($l);
                    }
                    $return[ 'last' ] = $last;
                    $return[ 'items' ] = $lists;
                }

                if (is_array($return) && count($return)) {
                    Output::i()->json($return);
                }
            } else {
                sleep(1);
                continue;
            }
        }
    }
}
