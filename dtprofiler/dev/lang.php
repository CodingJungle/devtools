<?php

$lang = [
    '__app_dtprofiler' => 'Dev Toolbox: Profiler',
    'dtprofiler_enabled_templates' => 'Templates',
    'dtprofiler_enabled_js' => 'JS',
    'dtprofiler_enabled_jsvars' => 'JSVars',
    'dtprofiler_enabled_css' => 'CSS',
    'dtprofiler_enabled_execution' => 'Execution',
    'dtprofiler_enabled_files' => 'Files',
    'dtprofiler_enabled_memory' => 'Memory',
    'dtprofiler_profiler_tabs_header' => 'Profiler Tabs',
    'dtprofiler_enabled_logs' => 'Logs',
    'dtprofiler_logs_amount' => 'Logs to Show',
    'dtprofiler_logs_amount_desc' => 'The number of logs to pull from the database.',
    'dtprofiler_enabled_enivro' => 'Environment',
    'dtprofiler_can_use' => 'Show To',
    'dtprofiler_can_use_desc' => 'The members who can see this when in_dev is set to false (useful for live sites)',
    'dtprofiler_devplus_tab' => 'Dev Center Plus',
    'dtprofiler_tab' => 'Profiler',
];
