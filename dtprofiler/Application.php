<?php
/**
 * @brief            Dev Toolbox: Profiler Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Profiler
 * @since            05 Apr 2018
 * @version          -storm_version-
 */

namespace IPS\dtprofiler;

/**
 * Dev Toolbox: Profiler Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
