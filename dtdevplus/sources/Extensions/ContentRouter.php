<?php

/**
 * @brief       ContentRouter Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Center Plus
 * @since       1.0.1
 * @version     -storm_version-
 */

namespace IPS\dtdevplus\Extensions;

use function count;
use function defined;
use function header;
use function implode;
use function is_array;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * Class _ContentRouter
 * @package IPS\dtdevplus\Extensions
 * @mixin \IPS\dtdevplus\Extensions\ExtensionsAbstract
 */
class _ContentRouter extends ExtensionsAbstract
{

    /**
     * @inheritdoc
     */
    protected function _content()
    {

        if (is_array($this->classRouter) && count($this->classRouter)) {
            $new = [];
            foreach ($this->classRouter as $class) {
                $new[] = '\\IPS\\' . $this->application->directory . '\\' . $class;
            }
            $this->classRouter = implode("','", $new);
        } else {
            $this->classRouter = \null;
        }
        return $this->_getFile($this->extension);
    }

    /**
     * @inheritdoc
     */
    public function elements(): array
    {

        $this->elements[] = [
            'name' => 'module',
        ];

        $this->elements[] = [
            'name' => 'classRouter',
            'class' => 'stack',
            'prefix' => '\\IPS\\' . $this->application->directory . '\\',
        ];

        return $this->elements;
    }
}
