<?php

/**
 * @brief       Debug Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  dtdevplus
 * @since       -storm_since_version-
 * @version     -storm_version-
 */

namespace IPS\dtdevplus\Profiler;

use function class_exists;
use function defined;
use function header;
use function method_exists;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * Class _Debug
 * @mixin \IPS\dtprofiler\Profiler\Debug
 */
class _Debug
{
    /**
     * Debug constructor.
     */
    public function __construct() { }

    public static function __callStatic($method, $args)
    {
        if (defined('\DTPROFILER') && \DTPROFILER && class_exists('\IPS\dtprofiler\Profiler\Debug')) {
            $class = \IPS\dtprofiler\Profiler\Debug::class;
            if (method_exists($class, $method)) {
                $class::{$method}(...$args);
            }
        }
    }
}
