<?php

/**
 * @brief       Dev Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Center Plus
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtdevplus;

use Exception;
use InvalidArgumentException;
use IPS\Application;
use IPS\dtbase\Forms;
use IPS\dtbase\ReservedWords;
use IPS\Http\Url;
use IPS\Output;
use IPS\Patterns\Singleton;
use IPS\Request;
use LogicException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use function array_pop;
use function count;
use function defined;
use function explode;
use function header;
use function is_array;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}


/**
 * @brief      _Dev Class
 * @mixin \IPS\dtdevplus\Dev
 */
class _Dev extends Singleton
{
    /**
     * @inheritdoc
     */
    protected static $instance;

    /**
     * The current application object
     * @var Application
     */
    protected $application;

    /**
     * application directory
     * @var null|string
     */
    protected $app;

    /**
     * _Sources constructor.
     * @throws InvalidArgumentException
     * @throws \OutOfRangeException
     */
    public function __construct()
    {
        try {
            $this->application = Application::load(Request::i()->appKey);
        } catch (Exception $e) {
            $this->application = Application::load('core');
        }

        $this->app = $this->application->directory;
        \IPS\dtbase\Application::loadAutoLoader();
    }

    /**
     * @return Forms|mixed
     * @throws Exception
     */
    public function form()
    {
        if (!Application::appIsEnabled('dtbase')) {
            Output::i()->error('Sorry you need to have the Devtoolbox: Base app installed to continue.', '2DT100');
        }

        /**
         * @var Forms $form ;
         */
        $config = [
            'elements' => $this->elements(),
        ];

        $form = Forms::execute($config);

        if ($values = $form->values()) {
            /**
             * @var \IPS\dtdevplus\Dev\Compiler\CompilerAbstract $class ;
             */
            $type = $values[ 'dtdevplus_dev_type' ];
            $class = 'IPS\\dtdevplus\\Dev\\Compiler';

            if ($type === 'template') {
                $class .= '\\Template';
            } else {
                $class .= '\\Javascript';
            }

            $class = new $class($values, $this->application);
            $class->process();
            $url = Url::internal("app=core&module=applications&controller=developer&appKey={$this->application->directory}&tab=DevFolder");
            Output::i()->redirect($url, 'foo');
        }

        return $form;
    }

    /**
     * @return array
     */
    public function elements(): array
    {
        $e = [];
        $e[ 'prefix' ] = 'dtdevplus_dev_';
        $e[] = [
            'class' => 'select',
            'name' => 'type',
            'ops' => [
                'options' => $this->_templateSelect(),
                'toggles' => $this->_toggles(),
            ],
        ];

        $groupManual = \true;
        try {
            $this->_getGroups($e);
            $groupManual = \false;
        } catch (InvalidArgumentException $msg) {
        } catch (IOException $e) {
        } catch (Exception $e) {
        }

        $groupManualJs = \true;
        try {
            $this->_getGroups($e, 'js');
            $groupManualJs = \false;
        } catch (InvalidArgumentException $msg) {
        } catch (IOException $e) {
        } catch (Exception $e) {
        }

        $e[] = [
            'class' => 'yn',
            'name' => 'group_manual',
            'default' => $groupManual,
            'ops' => [
                'togglesOn' => [
                    'group_manual_location',
                    'group_manual_folder',
                ],
            ],
        ];

        $e[] = [
            'name' => 'group_manual_js',
            'class' => 'yn',
            'default' => $groupManualJs,
            'ops' => [
                'togglesOn' => [
                    'group_manual_location',
                    'group_manual_folder',
                ],
            ],
        ];

        $e[] = [
            'name' => 'group_manual_location',
            'class' => 'select',
            'ops' => [
                'options' => [
                    'admin' => 'Admin',
                    'front' => 'Front',
                    'global' => 'Global',
                ],
            ],
        ];

        $e[] = [
            'name' => 'group_manual_folder',
            'required' => \true,
        ];


        $e[] = [
            'name' => 'filename',
            'required' => \true,
            'validation' => [$this, 'validateFilename'],
        ];

        $e[] = [
            'name' => 'arguments',
            'class' => 'stack',
        ];

        $e[] = [
            'name' => 'widgetname',
        ];

        return $e;
    }

    /**
     * @return array
     */
    protected function _templateSelect(): array
    {
        return [
            0 => 'Select Type',
            'template' => 'Template',
            'widget' => 'Widget',
            'module' => 'Module',
            'controller' => 'Controller',
        ];
    }

    /**
     * @return array
     */
    protected function _toggles(): array
    {
        return [
            'template' => [
                'html_group',
                'group_manual',
                'filename',
                'arguments',
            ],
            'widget' => [
                'js_group',
                'filename',
                'widgetname',
                'group_manual_js',
            ],
            'module' => [
                'js_group',
                'filename',
                'group_manual_js',
            ],
            'controller' => [
                'js_group',
                'filename',
                'group_manual_js',
            ],
        ];
    }

    /**
     * @param        $e
     * @param string $path
     * @throws InvalidArgumentException
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     * @throws \Exception
     */
    protected function _getGroups(&$e, $path = 'html')
    {
        $options = [];

        try {
            $base = \IPS\ROOT_PATH . \DIRECTORY_SEPARATOR . 'applications'.\DIRECTORY_SEPARATOR . $this->app . \DIRECTORY_SEPARATOR . 'dev' . \DIRECTORY_SEPARATOR . $path . \DIRECTORY_SEPARATOR;

            /* @var Finder $groups */
            $groups = new Finder;
            $fs = new Filesystem;
            $extended = '';
            if( $path === 'js'){
                $extended = \DIRECTORY_SEPARATOR.'controllers';
            }
            if ($fs->exists($base . 'admin'.$extended)) {
                $groups->in($base . 'admin' . $extended);
            }

            if ($fs->exists($base . 'front'.$extended)) {
                $groups->in($base . 'front'.$extended);
            }

            if ($fs->exists($base . 'global'.$extended)) {
                $groups->in($base . 'global'.$extended);
            }
            $groups->directories();
            foreach ($groups as $group) {
                $paths = $group->getRealPath();
                $paths = explode(\DIRECTORY_SEPARATOR, $paths);
                array_pop($paths);
                $location = array_pop($paths);
                if( $path === 'js' ){
                    $location = array_pop($paths);
                }
                if( \in_array( $location, ['front', 'global', 'admin'], \true ) ) {
                    $name = $location . ':' . $group->getFilename();
                    $options[ $name ] = $name;
                }
            }
        } catch (LogicException $es) {
        }

        if (is_array($options) && count($options)) {
            $e[] = [
                'class' => 'select',
                'name' => $path . '_group',
                'ops' => [
                    'options' => $options,
                ],
            ];
        } else {
            throw new InvalidArgumentException;
        }
    }

    /**
     * @param $data
     * @throws InvalidArgumentException
     * @throws \Exception
     */
    public function validateFilename($data)
    {
        if (Request::i()->dtdevplus_dev_type === 'template' && ReservedWords::check($data)) {
            throw new InvalidArgumentException('dtdevplus_class_reserved');
        }

        if (!$data) {
            throw new InvalidArgumentException('dtdevplus_class_no_blank');
        }
    }
}
