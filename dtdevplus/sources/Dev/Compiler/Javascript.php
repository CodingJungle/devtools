<?php

/**
 * @brief       Javascript Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Center Plus
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtdevplus\Dev\Compiler;

class _Javascript extends CompilerAbstract
{
    /**
     * @inheritdoc
     */
    public function content(): string
    {
        $module = 'ips.' . $this->app . '.' . $this->filename;
        $this->filename = $module . '.js';
        $type = 'controllers';
        $this->location .= '/' . $type;
        $find = ['{module}', '{widgetname}'];
        $replace = [$module, $this->widgetname];

        return $this->_replace($find, $replace, $this->_getFile($this->type));
    }
}
