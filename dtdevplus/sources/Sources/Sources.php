<?php

/**
 * @brief       Sources Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Center Plus
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtdevplus;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

use InvalidArgumentException;
use IPS\Application;
use IPS\Content\ClubContainer;
use IPS\Content\EditHistory;
use IPS\Content\Embeddable;
use IPS\Content\Featurable;
use IPS\Content\Followable;
use IPS\Content\FuturePublishing;
use IPS\Content\Hideable;
use IPS\Content\Lockable;
use IPS\Content\MetaData;
use IPS\Content\Pinnable;
use IPS\Content\Polls;
use IPS\Content\Reactable;
use IPS\Content\ReadMarkers;
use IPS\Content\Reportable;
use IPS\Content\Searchable;
use IPS\Content\Shareable;
use IPS\Content\Tags;
use IPS\Content\Views;
use IPS\Db;
use IPS\dtbase\Forms;
use IPS\dtbase\ReservedWords;
use IPS\Http\Url;
use IPS\Member;
use IPS\Node\Colorize;
use IPS\Node\Permissions;
use IPS\Node\Ratings;
use IPS\Output;
use IPS\Patterns\Singleton;
use IPS\Request;
use OutOfRangeException;
use SplObserver;
use function array_keys;
use function array_merge;
use function class_exists;
use function count;
use function defined;
use function file_exists;
use function header;
use function in_array;
use function interface_exists;
use function is_array;
use function mb_strtolower;
use function trait_exists;

class _Sources extends Singleton
{
    /**
     * @inheritdoc
     */
    protected static $instance;

    /**
     * The current application object
     * @var Application
     */
    protected $application;

    /**
     * _Sources constructor.
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        try {
            $this->application = Application::load(Request::i()->appKey ?? 'core');
        } catch (OutOfRangeException $e) {
        }
    }

    /**
     * returns the form
     * @return Forms
     * @throws \Exception
     */
    public function form(): \IPS\Helpers\Form
    {
        if (!Application::appIsEnabled('dtbase')) {
            Output::i()->error('Sorry you need to have the Devtoolbox: Base app installed to continue.', '2DT100');
        }

        /**
         * @var Forms $form ;
         */
        $config = [
            'elements' => $this->elements(),
        ];

        $form = Forms::execute($config);

        if ($values = $form->values()) {
            /* @var Application $app */
            foreach (Application::allExtensions('dtdevplus', 'SourcesFormAbstract') as $app) {
                /* @var Sources\SourcesFormAbstract $extension */
                foreach ($app->extensions('dtdevplus', 'SourcesFormAbstract') as $extension) {
                    $extension->formProcess($values);
                }
            }

            /* @var Sources\Generator\GeneratorAbstract $class */
            $class = 'IPS\\dtdevplus\\Sources\\Generator\\';
            //$class = 'IPS\\dtdevplus\\Sources\\Compiler\\';
            $type = $values[ 'dtdevplus_class_type' ];
            switch ($type) {
                case 'Memory':
                case 'Debug':
                    $class .= 'Profiler';
                    $values[ 'dtdevplus_class_className' ] = mb_ucfirst($type);
                    $values[ 'dtdevplus_class_namespace' ] = 'Profiler';
                    break;
                case 'Form':
                    $class .= 'Form';
                    $values[ 'dtdevplus_class_className' ] = 'Form';
                    $values[ 'dtdevplus_class_namespace' ] = '';
                    break;
                default:
                    $class .= mb_ucfirst($type);
                    break;
            }

            $inter = [];
            $traits = [];

            if ($type === 'Node') {
                $inter = is_array($values[ 'dtdevplus_class_interface_implements_node' ]) ? $values[ 'dtdevplus_class_interface_implements_node' ] : [];
                unset($values[ 'dtdevplus_class_interface_implements_node' ]);
            }

            if ($type === 'Item') {
                $inter = is_array($values[ 'dtdevplus_class_interface_implements_item' ]) ? $values[ 'dtdevplus_class_interface_implements_item' ] : [];
                unset($values[ 'dtdevplus_class_interface_implements_item' ]);
            }

            if (in_array($type, ['Comment', 'Review'])) {
                $inter = is_array($values[ 'dtdevplus_class_interface_implements_comment' ]) ? $values[ 'dtdevplus_class_interface_implements_comment' ] : [];
                unset($values[ 'dtdevplus_class_interface_implements_comment' ]);
            }

            if (is_array($values[ 'dtdevplus_class_implements' ])) {
                $values[ 'dtdevplus_class_implements' ] = array_merge($values[ 'dtdevplus_class_implements' ], $inter);
            }

            if ($type === 'Node') {
                $traits = is_array($values[ 'dtdevplus_class_ips_traits_node' ]) ? $values[ 'dtdevplus_class_ips_traits_node' ] : [];
                unset($values[ 'dtdevplus_class_ips_traits_node' ]);
            }

            if (in_array($type, ['Item', 'Review', 'Comment'])) {
                $traits = is_array($values[ 'dtdevplus_class_ips_traits_item' ]) ? $values[ 'dtdevplus_class_ips_traits_item' ] : [];
                unset($values[ 'dtdevplus_class_ips_traits_item' ]);
            }

            if (is_array($values[ 'dtdevplus_class_traits' ])) {
                $values[ 'dtdevplus_class_traits' ] = array_merge($values[ 'dtdevplus_class_traits' ], $traits);
            }

            $class = new $class($values, $this->application);
            $class->process();

            if (!$class->error) {
                $msg = Member::loggedIn()
                             ->language()
                             ->addToStack('dtdevplus_class_created', \false, ['sprintf' => [$type, $class->classname]]);
            } else {
                $msg = Member::loggedIn()
                             ->language()
                             ->addToStack('dtdevplus_class_db_error', \false,
                                 ['sprintf' => ['type', $class->classname, $class->database]]);
            }

            $url = Url::internal("app=core&module=applications&controller=developer&appKey={$this->application->directory}&tab=ClassDev");
            Output::i()->redirect($url, $msg);
        }

        return $form;
    }

    /**
     * the elements for the form, use the forms class.
     * @return array
     */
    public function elements(): array
    {
        $e[] = [
            'class' => 'Select',
            'name' => 'type',
            'default' => 'select',
            'required' => \true,
            'validation' => [$this, 'selectCheck'],
            'options' => [
                'options' => $this->classType(),
                'toggles' => $this->toggles(),
            ],
        ];
        $e[] = [
            'name' => 'namespace',
            'options' => [
                'placeholder' => 'Namespace',
            ],
            'prefix' => "IPS\\{$this->application->directory}\\",
        ];

        $e[] = [
            'name' => 'className',
            'required' => \true,
            'options' => [
                'placeholder' => 'Class Name',
            ],
            'validation' => [$this, 'classCheck'],
            'prefix' => '_',
        ];

        $e[] = [
            'name' => 'useImports',
            'class' => 'yn',
            'default' => \true,
        ];

        $e[] = [
            'name' => 'database',
            'prefix' => $this->application->directory . '_',
        ];

        $e[] = [
            'name' => 'prefix',
            'suffix' => '_',
        ];

        $e[] = [
            'class' => 'yn',
            'name' => 'scaffolding_create',
            'default' => \true,
        ];

        $e[] = [
            'class' => 'yn',
            'name' => 'abstract',
            'default' => \false,
        ];

        $e[] = [
            'class' => 'yn',
            'name' => 'final',
            'default' => \false,
        ];


        $e[] = [
            'name' => 'item_node_class',
            'prefix' => "IPS\\{$this->application->directory}\\",
        ];

        $e[] = [
            'name' => 'comment_class',
            'prefix' => "IPS\\{$this->application->directory}\\",
        ];

        $e[] = [
            'name' => 'review_class',
            'prefix' => "IPS\\{$this->application->directory}\\",
        ];

        $e[] = [
            'name' => 'content_item_class',
            'prefix' => "IPS\\{$this->application->directory}\\",
        ];

        $e[] = [
            'name' => 'extends',
            'validation' => [$this, 'extendsCheck'],
        ];

        $interfacesNode = [
            Permissions::class => Permissions::class,
            Ratings::class => Ratings::class,
        ];
        $interfacesItem = [
            EditHistory::class => EditHistory::class,
            Embeddable::class => Embeddable::class,
            Featurable::class => Featurable::class,
            Followable::class => Followable::class,
            FuturePublishing::class => FuturePublishing::class,
            Hideable::class => Hideable::class,
            Lockable::class => Lockable::class,
            MetaData::class => MetaData::class,
            \IPS\Content\Permissions::class => \IPS\Content\Permissions::class,
            Pinnable::class => Pinnable::class,
            Polls::class => Polls::class,
            SplObserver::class => SplObserver::class,
            \IPS\Content\Ratings::class => \IPS\Content\Ratings::class,
            ReadMarkers::class => ReadMarkers::class,
            Searchable::class => Searchable::class,
            Shareable::class => Shareable::class,
            Tags::class => Tags::class,
            Views::class => Views::class,
        ];
        $interfacesComment = [
            Hideable::class => Hideable::class,
            Embeddable::class => Embeddable::class,
            Searchable::class => Searchable::class,
            Lockable::class => Lockable::class,
            EditHistory::class => EditHistory::class,
        ];
        $traitsNode = [
            ClubContainer::class => ClubContainer::class,
            Colorize::class => Colorize::class,
        ];
        $traitsItems = [
            Reactable::class => Reactable::class,
            Reportable::class => Reportable::class,
        ];


        $e[] = [
            'class' => 'checkboxset',
            'name' => 'interface_implements_node',
            'default' => array_keys($interfacesNode),
            'ops' => [
                'options' => $interfacesNode,
            ],
        ];

        $e[] = [
            'class' => 'checkboxset',
            'name' => 'interface_implements_item',
            'default' => array_keys($interfacesItem),
            'ops' => [
                'options' => $interfacesItem,
            ],
        ];

        $e[] = [
            'class' => 'checkboxset',
            'name' => 'interface_implements_comment',
            'default' => array_keys($interfacesComment),
            'ops' => [
                'options' => $interfacesComment,
            ],
        ];

        $e[] = [
            'class' => 'stack',
            'name' => 'implements',
            'validation' => [$this, 'implementsCheck'],
        ];

        $e[] = [
            'class' => 'checkboxset',
            'name' => 'ips_traits_node',
            'default' => array_keys($traitsNode),
            'ops' => [
                'options' => $traitsNode,
            ],
        ];

        $e[] = [
            'class' => 'checkboxset',
            'name' => 'ips_traits_item',
            'default' => array_keys($traitsItems),
            'ops' => [
                'options' => $traitsItems,
            ],
        ];

        $e[] = [
            'class' => 'stack',
            'name' => 'traits',
            'validation' => [$this, 'traitsCheck'],
        ];

        $e[] = [
            'name' => 'interfaceName',
            'validate' => [$this, 'interfaceClassCheck'],
        ];

        $e[] = [
            'name' => 'traitName',
            'validate' => [$this, 'traitClassCheck'],
        ];

        $e[ 'prefix' ] = 'dtdevplus_class_';
        return $e;
    }

    /**
     * the class types dtdevplus supports
     * @return array
     */
    protected function classType(): array
    {
        $classTypes = [
            'select' => 'Select Type',
            'Standard' => 'Standard Class',
            'Interfacing' => 'Interface',
            'Traits' => 'Trait',
            'Singleton' => 'Singleton',
            'ActiveRecord' => 'Active Record',
            'Node' => 'Node',
            'Item' => 'Item',
            'Comment' => 'Comment',
            'Review' => 'Review',
            'Memory' => 'Profiler Memory',
            'Debug' => 'Profiler Debug',
//            'Meta' => 'Meta Class',
            'Form' => 'Form Class',
//            'Faform' => 'FA Form Helper'
        ];

        $debug = '\\IPS\\'.$this->application->directory.'\\Profiler\\Debug';
        $memory = '\\IPS\\'.$this->application->directory.'\\Profiler\\Memory';
        if( class_exists( $debug, \true ) ){
            unset( $classTypes['Debug'] );
        }

        if( class_exists( $memory, \true ) ){
            unset( $classTypes['Memory'] );
        }

        return $classTypes;
    }

    /**
     * returns the toggle list for the form
     * @return array
     */
    protected function toggles(): array
    {
        $toggles = [
            'Standard' => [
                'namespace',
                'className',
                'extends',
                'implements',
                'traits',
                'abstract',
                'final',
            ],
            'Interfacing' => [
                'namespace',
                'interfaceName',
            ],
            'Traits' => [
                'namespace',
                'traitName',
            ],
            'Singleton' => [
                'namespace',
                'className',
                'implements',
                'traits',
                'useImports',
            ],
            'ActiveRecord' => [
                'namespace',
                'className',
                'database',
                'prefix',
                'scaffolding_create',
                'useImports',
            ],
            'Node' => [
                'namespace',
                'className',
                'implements',
                'database',
                'prefix',
                'traits',
                'content_item_class',
                'ips_traits_node',
                'interface_implements_node',
                'scaffolding_create',
                'useImports',
            ],
            'Item' => [
                'node',
                'namespace',
                'className',
                'implements',
                'item_node_class',
                'database',
                'prefix',
                'traits',
                'review_class',
                'comment_class',
                'interface_implements_item',
                'ips_traits_item',
                'scaffolding_create',
                'useImports',
            ],
            'Comment' => [
                'namespace',
                'className',
                'implements',
                'content_item_class',
                'database',
                'prefix',
                'traits',
                'interface_implements_comment',
                'ips_traits_item',
                'scaffolding_create',
                'useImports',
            ],
            'Review' => [
                'namespace',
                'className',
                'implements',
                'content_item_class',
                'database',
                'prefix',
                'traits',
                'interface_implements_comment',
                'ips_traits_item',
                'scaffolding_create',
                'useImports',
            ],
        ];

        return $toggles;
    }

    /**
     * checks to see if the class doesn't exist and the classname is good
     * @throws InvalidArgumentException
     * @param $data
     * @throws InvalidArgumentException
     */
    public function classCheck($data)
    {
        $ns = mb_ucfirst(Request::i()->dtdevplus_class_namespace);
        $class = mb_ucfirst($data);
        $class = $ns ? '\\IPS\\' . $this->application->directory . '\\' . $ns . '\\' . $class : '\\IPS\\' . $this->application->directory . '\\' . $class;

        if ($data !== 'Form' && class_exists($class)) {
            throw new InvalidArgumentException('dtdevplus_class_exists');
        }

        if (ReservedWords::check($data)) {
            throw new InvalidArgumentException('dtdevplus_class_reserved');
        }
    }

    /**
     * checks to see if the trait doesn't exist and the trait name is good!
     * @throws InvalidArgumentException
     * @param $data
     */
    public function traitClassCheck($data)
    {
        $ns = mb_ucfirst(Request::i()->dtdevplus_class_namespace);
        $class = mb_ucfirst($data);
        if ($ns) {
            $class = '\\IPS\\' . $this->application->directory . '\\' . $ns . '\\' . $class;
        } else {
            $class = '\\IPS\\' . $this->application->directory . '\\' . $class;
        }

        if (trait_exists($class)) {
            throw new InvalidArgumentException('dtdevplus_class_trait_exists');
        }

        if (ReservedWords::check($data)) {
            throw new InvalidArgumentException('dtdevplus_class_reserved');
        }
    }

    /**
     * checks to see if the interface doesn't exist and the name is good!
     * @throws InvalidArgumentException
     * @param $data
     */
    public function interfaceClassCheck($data)
    {
        $ns = mb_ucfirst(Request::i()->dtdevplus_class_namespace);
        $class = mb_ucfirst($data);
        if ($ns) {
            $class = "\\IPS\\" . $this->application->directory . "\\" . $ns . "\\" . $class;
        } else {
            $class = "\\IPS\\" . $this->application->directory . "\\" . $class;
        }

        if (interface_exists($class)) {
            throw new InvalidArgumentException('dtdevplus_class_interface_exists');
        }

        if (ReservedWords::check($data)) {
            throw new InvalidArgumentException('dtdevplus_class_reserved');
        }
    }

    /**
     * checks to see if the Class/Trait/Interface name isn't blank!
     * @throws InvalidArgumentException
     * @param $data
     */
    public function noBlankCheck($data)
    {
        if (!$data) {
            throw new InvalidArgumentException('dtdevplus_class_no_blank');
        }
    }

    /**
     * checks the parent class exist if one is provided
     * @throws InvalidArgumentException
     * @param $data
     */
    public function extendsCheck($data)
    {
        if ($data && !class_exists($data, \true)) {
            throw new InvalidArgumentException('dtdevplus_class_extended_class_no_exist');
        }
    }

    /**
     * Checks to make sure the interface files exist
     * @throws InvalidArgumentException
     * @param $data
     */
    public function implementsCheck($data)
    {
        if (is_array($data) && count($data)) {
            foreach ($data as $implement) {
                if ($implement === \IPS\Content\Reportcenter::class && Application::load('core')->long_version >= '103000') {
                    throw new InvalidArgumentException('\IPS\Content\Reportcenter is no longer supported in IPS 4.3+');
                }

                if (!interface_exists($implement)) {
                    $lang = Member::loggedIn()
                                  ->language()
                                  ->addToStack('dtdevplus_class_implemented_no_interface', \false,
                                      ['sprintf' => $implement]);
                    Member::loggedIn()->language()->parseOutputForDisplay($lang);
                    throw new InvalidArgumentException($lang);
                }
            }
        }
    }

    /**
     * checks to make sure the traits being used exists
     * @throws InvalidArgumentException
     * @param $data
     */
    public function traitsCheck($data)
    {
        if (is_array($data) && count($data)) {
            foreach ($data as $trait) {
                if (!trait_exists($trait)) {
                    $lang = Member::loggedIn()
                                  ->language()
                                  ->addToStack('dtdevplus_class_no_trait', \false, ['sprintf' => [$trait]]);
                    Member::loggedIn()->language()->parseOutputForDisplay($lang);
                    throw new InvalidArgumentException($lang);
                }
            }
        }
    }

    /**
     * checks to make sure the node exist for the content item class.
     * @throws InvalidArgumentException
     * @param $data
     */
    public function itemNodeCheck($data)
    {
        if ($data) {
            $class = "IPS\\{$this->application->directory}\\{$data}";
            if (!class_exists($class)) {
                throw new InvalidArgumentException('dtdevplus_class_node_item_missing');
            }

            if (ReservedWords::check($data)) {
                throw new InvalidArgumentException('dtdevplus_class_reserved');
            }
        }
    }

    /**
     * checks to make sure a class type is selected
     * @throws InvalidArgumentException
     * @param $data
     */
    public function selectCheck($data)
    {
        if ($data === 'select') {
            throw new InvalidArgumentException('dtdevplus_class_type_no_selection');
        }
        /* @var Sources\Generator\GeneratorAbstract $class */
        $class = 'IPS\\dtdevplus\\Sources\\Generator\\';
        $type = Request::i()->dtdevplus_class_type;
        switch ($type) {
            case 'Memory':
            case 'Debug':
                $class .= 'Profiler';
                $values[ 'dtdevplus_class_className' ] = mb_ucfirst($type);
                $values[ 'dtdevplus_class_namespace' ] = 'Profiler';
                break;
            default:
                $class .= mb_ucfirst($type);
                break;
        }

        //is this ours?
        if (!class_exists($class, \true)) {
            $class = $type;
            if (!class_exists($class, \true)) {
                throw new InvalidArgumentException('This Class ' . $class . ' doesn\'t exist!');
            }
        }
        $app = $this->application->directory;
        if (Request::i()->dtdevplus_class_scaffolding_create) {
            if (Request::i()->dtdevplus_class_database) {
                $db = $app . '_' . Request::i()->dtdevplus_class_database;
            } else {
                $db = $app . '_' . mb_strtolower(Request::i()->dtdevplus_class_className);
            }

            if (Db::i()->checkForTable($db)) {
                throw new InvalidArgumentException('This db, ' . $db . ', already exists! please change or uncheck create scaffolding.');
            }

            $path = \IPS\ROOT_PATH . '/applications/' . $app . '/modules/';
            if (Request::i()->dtdevplus_class_type === 'node') {
                $lower = mb_strtolower(Request::i()->dtdevplus_class_className);
                $path .= 'admin/' . $lower . '/' . $lower . '.php';
                if (file_exists($path)) {
                    throw new InvalidArgumentException('The controller,' . $lower . '.php already exist, please uncheck create scaffolding');
                }
            }
        }
    }
}
