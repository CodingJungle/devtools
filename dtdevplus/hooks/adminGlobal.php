//<?php

use IPS\Request;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtdevplus_hook_adminGlobal extends _HOOK_CLASS_
{

    /* !Hook Data - DO NOT REMOVE */
    public static function hookData()
    {
        return parent::hookData();
    }

    /* End Hook Data */
    public function tabs($tabNames, $activeId, $defaultContent, $url, $tabParam = 'tab')
    {
        if (Request::i()->app === "core" && Request::i()->module === "applications" && Request::i()->controller === "developer" && !Request::i()->do) {
            $tabNames[ 'ClassDev' ] = 'dtdevplus_dev_class';
            $tabNames[ 'DevFolder' ] = 'dtdevplus_dev_folder';
            $tabNames[ 'StormLangs' ] = 'dtdevplus_dev_lang';
        }

        return parent::tabs($tabNames, $activeId, $defaultContent, $url, $tabParam);

    }
}
