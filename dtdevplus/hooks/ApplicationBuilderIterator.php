//<?php

if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtdevplus_hook_ApplicationBuilderIterator extends _HOOK_CLASS_
{

    /**
     * Current value
     *
     * @return    void
     */
    public function current()
    {
        $file = (string)parent::current();

        if (\mb_substr(\str_replace('\\', '/', $file),
                \mb_strlen(\IPS\ROOT_PATH . "/applications/" . $this->application->directory) + 1, 6) === 'hooks/') {
            $temporary = \tempnam(\IPS\TEMP_DIRECTORY, 'IPS');

            \file_put_contents($temporary, \IPS\Plugin::addExceptionHandlingToHookFile($file));

            \register_shutdown_function(function ($temporary) {
                \unlink($temporary);
            }, $temporary);

            return $temporary;
        }

        if (\is_file($file) && (\mb_strpos($file, '3rdparty') === \false || \mb_strpos($file,
                    '3rd_party') === \false || \mb_strpos($file, 'vendor') === \false)) {
            if (!\IPS\dtdevplus\Headerdoc::i()->can($this->application)) {
                return $file;
            }

            $path = new \SplFileInfo($file);

            if ($path->getExtension() === 'php') {
                $temporary = \tempnam(\IPS\TEMP_DIRECTORY, 'IPS');
                $contents = \file_get_contents($file);

                /* @var \IPS\dtdevplus\extensions\dtdevplus\Headerdoc\Headerdoc $class */
                foreach ($this->application->extensions('dtdevplus', 'Headerdoc', \true) as $class) {
                    if (\method_exists($class, 'finalize')) {
                        $contents = $class->finalize($contents, $this->application);
                    }
                }

                \file_put_contents($temporary, $contents);

                \register_shutdown_function(function ($temporary) {
                    \unlink($temporary);
                }, $temporary);

                return $temporary;
            }
        }

        return $file;
    }
}
