//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */

use IPS\Application;
use IPS\Request;
use IPS\Settings;

if (!\defined('\IPS\SUITE_UNIQUE_KEY')) {
    exit;
}

class dtdevplus_hook_ApplicationBuilderFilter extends _HOOK_CLASS_
{
    public function accept()
    {
        if ($this->isFile()) {
            $skip = [];
            $toSKip = \json_decode(Settings::i()->dtdevplus_skip_files, \true);

            if (\is_array($toSKip) && \count($toSKip)) {
                $skip = $toSKip;
            }

            try {
                $appKey = Request::i()->appKey;
                $app = Application::load($appKey);

                foreach ($app->extensions('dtdevplus', 'Headerdoc', \true) as $class) {
                    if (\method_exists($class, 'filesSkip')) {
                        $class->filesSkip($skip);
                    }
                }
            } catch (\Exception $e) {
            }

            return !\in_array($this->getFilename(), $skip, \true);
        }

        return parent::accept();
    }

    protected function getDirectoriesToIgnore()
    {
        $skip = parent::getDirectoriesToIgnore();
        $appKey = Request::i()->appKey;

        if (\in_array($appKey, \IPS\dtbase\Application::$toolBoxApps, \true)) {
            foreach ($skip as $key => $val) {
                if ($val === 'dev') {
                    unset($skip[ $key ]);
                }
            }
        }
        $toSKip = \json_decode(Settings::i()->dtdevplus_skip_dirs, \true);

        if (\is_array($toSKip) && \count($toSKip)) {
            $skip = \array_merge($skip, $toSKip);
        }

        try {
            $app = Application::load($appKey);

            /* @var \IPS\dtdevplus\extensions\dtdevplus\Headerdoc\Headerdoc $class */
            foreach ($app->extensions('dtdevplus', 'Headerdoc', \true) as $class) {
                if (\method_exists($class, 'dirSkip')) {
                    $class->dirSkip($skip);
                }
            }
        } catch (\Exception $e) {

        }


        return $skip;
    }
}
