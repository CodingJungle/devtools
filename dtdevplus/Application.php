<?php
/**
 * @brief            Dev Toolbox: Dev Center Plus Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Dev Center Plus
 * @since            25 Mar 2018
 * @version          -storm_version-
 */

namespace IPS\dtdevplus;

/**
 * Dev Toolbox: Dev Center Plus Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
