<?php

/**
 * @brief       Dtbase Settings extension: Headerdoc
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Center Plus
 * @since       1.1.0
 * @version     -storm_version-
 */

namespace IPS\dtdevplus\extensions\dtbase\Settings;

use IPS\Settings;
use function defined;
use function header;
use function is_array;
use function json_decode;
use function json_encode;

/* To prevent PHP errors (extending class does not exist) revealing path */

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * headerdoc
 */
class _headerdoc
{

    /**
     * add in array of form helpers
     * @param array $helpers
     */
    public function elements(&$helpers)
    {
        $helpers[] = [
            'name' => 'dtdevplus_skip_files',
            'class' => 'stack',
            'default' => json_decode(Settings::i()->dtdevplus_skip_files, \true),
        ];

        $helpers[] = [
            'name' => 'dtdevplus_skip_dirs',
            'class' => 'stack',
            'default' => json_decode(Settings::i()->dtdevplus_skip_dirs, \true),
        ];
    }

    /**
     * return a tab name
     * @return string
     */
    public function tab()
    {
        return 'dtdevplus';
    }

    /**
     * formValues, format the values before saving as settings
     * @param array $values
     * @return void
     */
    public function formateValues(&$values)
    {
        if (is_array($values[ 'dtdevplus_skip_files' ])) {
            $values[ 'dtdevplus_skip_files' ] = json_encode($values[ 'dtdevplus_skip_files' ]);
        } else {
            $values[ 'dtdevplus_skip_files' ] = '{}';
        }

        if (is_array($values[ 'dtdevplus_skip_dirs' ])) {
            $values[ 'dtdevplus_skip_dirs' ] = json_encode($values[ 'dtdevplus_skip_dirs' ]);
        } else {
            $values[ 'dtdevplus_skip_dirs' ] = '{}';
        }
    }
}
