<?php

/**
 * @brief       Plugins Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Folders
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtdevfolder;

use IPS\dtbase\Generator\DTFileGenerator;
use IPS\dtbase\Shared\Write;
use IPS\Http\Url;
use IPS\Member;
use IPS\Output;
use IPS\Patterns\Singleton;
use IPS\Xml\XMLReader;
use Symfony\Component\Filesystem\Filesystem;
use function base64_decode;
use function count;
use function defined;
use function header;
use function json_encode;
use function mb_strtolower;
use function preg_replace;
use function unlink;
use function var_export;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

class _Plugins extends Singleton
{
    use Write;

    public static $instance;

    /**
     * @param bool $file
     * @throws \RuntimeException
     */
    public function finish($file = \false)
    {
        $return = $this->build($file);
        @unlink($file);

        $message = Member::loggedIn()
                         ->language()
                         ->addToStack($return[ 'msg' ], \false, ['sprintf' => [$return[ 'name' ]]]);
        $url = Url::internal('app=dtdevfolder&module=view&controller=plugins');
        Output::i()->redirect($url, $message);
    }

    /**
     * @param $plugin
     * @return array
     * @throws \RuntimeException
     */
    public function build($plugin): array
    {
        \IPS\dtbase\Application::loadAutoLoader();
        $fs = new Filesystem;
        $xml = new XMLReader;
        $xml->open($plugin);
        $xml->read();
        $plugins = \IPS\ROOT_PATH . '/plugins/';
        $versions = [];
        $lang = [];
        $langJs = [];
        $settings = [];
        $return = 'dtdevfolder_plugins_done';
        $oriName = $xml->getAttribute('name');
        $xml->getAttribute('author');
        $name = mb_strtolower(preg_replace('#[^a-zA-Z0-9_]#', '', $oriName));
        $pluginName = $oriName;
        $folder = $plugins . $name . '/dev/';
        $html = $folder . 'html/';
        $css = $folder . 'css/';
        $js = $folder . 'js/';
        $resources = $folder . 'resources/';
        $setup = $folder . 'setup/';
        $widgets = [];
        while ($xml->read()) {
            if ($xml->nodeType !== XMLReader::ELEMENT) {
                continue;
            }
            if ($xml->name === 'html') {
                $filename = $xml->getAttribute('filename');
                $content = base64_decode($xml->readString());
                $this->_writeFile($filename, $content, $html);
            }

            if ($xml->name === 'css') {
                $filename = $xml->getAttribute('filename');
                $content = base64_decode($xml->readString());
                $this->_writeFile($filename, $content, $css);
            }

            if ($xml->name === 'js') {
                $filename = $xml->getAttribute('filename');
                $content = base64_decode($xml->readString());
                $this->_writeFile($filename, $content, $js);
            }

            if ($xml->name === 'resources') {
                $filename = $xml->getAttribute('filename');
                $content = base64_decode($xml->readString());
                $this->_writeFile($filename, $content, $resources);
            }

            if ($xml->name === 'version') {
                $versions[ $xml->getAttribute('long') ] = $xml->getAttribute('human');
                $content = $xml->readString();
                if ($xml->getAttribute('long') === '10000') {
                    $name = 'install.php';
                } else {
                    $name = $xml->getAttribute('long') . '.php';
                }
                $this->_writeFile($name, $content, $setup);
            }

            if ($xml->name === 'setting') {
                $xml->read();
                $key = $xml->readString();
                $xml->next();
                $value = $xml->readString();
                $settings[] = ['key' => $key, 'default' => $value];
            }

            if ($xml->name === 'word') {
                $key = $xml->getAttribute('key');
                $value = $xml->readString();
                $jsW = (int)$xml->getAttribute('js');

                if ($jsW) {
                    $langJs[ $key ] = $value;
                } else {
                    $lang[ $key ] = $value;
                }
            }

            if ($xml->name === 'widget') {
                $widgets[ $xml->getAttribute('key') ] = [
                    'class' => $xml->getAttribute('class'),
                    'restrict' => $xml->getAttribute('restrict'),
                    'default_area' => $xml->getAttribute('default_area'),
                    'allow_reuse' => $xml->getAttribute('allow_reuse') === 1,
                    'menu_style' => $xml->getAttribute('menu_style'),
                    'embeddable' => $xml->getAttribute('embeddable') === 1,
                ];
            }
        }

        if (count($widgets)) {
            $content = json_encode($widgets, \JSON_PRETTY_PRINT);
            $this->_writeFile('widgets.json', $content, $folder);
        }

        $content = json_encode($settings, \JSON_PRETTY_PRINT);
        $this->_writeFile('settings.json', $content, $folder);
        $content = json_encode($versions, \JSON_PRETTY_PRINT);
        $this->_writeFile('versions.json', $content, $folder);

        $langFile = new DTFileGenerator;
        $langFile->setFilename($folder . '/lang.php');
        $langFile->setBody('$lang=' . var_export($lang, \true) . ";");
        $langFile->write();

        $langFile->setFilename($folder . '/jslang.php');
        $langFile->setBody('$lang=' . var_export($langJs, \true) . ";");
        $langFile->write();

        return ['msg' => $return, 'name' => $pluginName];
    }
}
