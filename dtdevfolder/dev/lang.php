<?php

$lang = [
    '__app_dtdevfolder' => 'Dev Toolbox: Dev Folders',
    'menu__dtdevfolder_view' => 'Dev Folders',
    'menu__dtdevfolder_view_apps' => 'Application',
    'dtdevfolder_apps_select' => 'Select Application',
    'dtdevfolder_app' => 'Application',
    'dtdevfolder_app_desc' => 'Select which application to generate the Dev Folder for.',
    'dtdevfolder_type' => 'Type',
    'dtdevfolder_type_desc' => 'Choose everything to recreate the entire Dev Folder, or select which part of the Dev folder you wish to recreate.',
    'dtdevfolder_type_lang' => 'Language',
    'dtdevfolder_type_js' => 'Javascript',
    'dtdevfolder_type_template' => 'Templates',
    'dtdevfolder_type_email' => 'Email Templates',
    'dtdevfolder_type_all' => 'Everything',
    'dtdevfolder_type_select' => 'Select Type',
    'dtdevfolder_return_javascript' => 'Javascript Files Generated',
    'dtdevfolder_return_templates' => 'Template Files Generated',
    'dtdevfolder_return_email' => 'Email Template Files Generated',
    'dtdevfolder_return_lang' => 'Language Strings Generated',
    'dtdevfolder_queue_title' => 'Generating Dev Files',
    'dtdevfolder_total_done' => 'Processing %s of %s',
    'dtdevfolder_completed' => 'Dev folder generated for %s',
    'menu__dtdevfolder_devplus' => 'Dev Folder Generator',
    'menu__dtdevfolder_devplus_devplus' => 'Applications',
    'dtdevfolder_title' => 'Application Dev Folder Generator',
    'dtdevfolder_plugins_title' => 'Plugins Dev Folder Generator',
    'dtdevfolder_plugin_upload' => 'Plugin XML',
    'dtdevfolder_plugins_done' => 'The Dev Folder for %s has been generated!',
    'menu__dtdevfolder_view_plugins' => 'Plugins',
    'dtdevplus_ext_use_default' => 'Use Default',
    'dtdevplus_ext_use_default_desc' => 'Will ignore all other settings on the page and build and IPS/App extensions skeleton!',
];
