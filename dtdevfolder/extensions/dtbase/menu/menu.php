<?php

/**
 * @brief       Dtbase Menu extension: Menu
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Dev Folders
 * @since       1.0.0
 * @version     -storm_version-
 */

namespace IPS\dtdevfolder\extensions\dtbase\Menu;

use IPS\Http\Url;
use function defined;
use function header;

/* To prevent PHP errors (extending class does not exist) revealing path */

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * menu
 */
class _menu
{

    /**
     * return an array for the menu
     * @param $menus
     */
    public function menu(&$menus)
    {
        $menus[ 'roots' ][ 'dtbase' ] = [
            'id' => 'dtbase',
            'name' => 'Dev Toolbox',
            'url' => 'elDevToolsDTbase',
        ];

        $menus[ 'dtbase' ][] = [
            'id' => 'proxy',
            'name' => 'Generate Application Dev Folder',
            'url' => (string)Url::internal('app=dtdevfolder&module=view&controller=apps'),
        ];

        $menus[ 'dtbase' ][] = [
            'id' => 'proxy',
            'name' => 'Generate Plugin Dev Folder',
            'url' => (string)Url::internal('app=dtdevfolder&module=view&controller=plugins'),
        ];
    }
}
