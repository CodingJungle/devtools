<?php
/**
 * @brief            Dev Toolbox: Dev Folders Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Dev Folders
 * @since            26 Mar 2018
 * @version          -storm_version-
 */

namespace IPS\dtdevfolder;

/**
 * Dev Toolbox: Dev Folders Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
