<?php

/**
 * @brief       Dtproxy Proxyhelpers extension: Proxy
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Proxy Class Generator
 * @since       1.1.0
 * @version     -storm_version-
 */

namespace IPS\dtproxy\extensions\dtproxy\ProxyHelpers;

use IPS\Data\Store;
use IPS\dtdevplus\Sources\Generator\GeneratorAbstract as devPlusGeneratorAbstract;
use IPS\dtproxy\Helpers\GeneratorAbstract;
use IPS\dtproxy\Helpers\Request as HelpersRequest;
use IPS\dtproxy\Helpers\Store as HelpersStore;
use IPS\Node\Model;
use IPS\Output;
use IPS\Request;
use function defined;
use function header;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header(($_SERVER[ 'SERVER_PROTOCOL' ] ?? 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * proxy
 */
class _proxy
{

    /**
     * add property to \IPS\Data\Store DocComment
     * @param array $classDoc
     */
    public function store(&$classDoc)
    {
        $classDoc[] = ['pt' => 'p', 'prop' => 'dtproxy_proxy_files', 'type' => 'array'];
        $classDoc[] = ['pt' => 'p', 'prop' => 'dt_json', 'type' => 'array'];
        $classDoc[] = ['pt' => 'p', 'prop' => 'dtproxy_templates', 'type' => 'array'];
    }

    /**
     * add property to \IPS\Request proxy DocComment
     * @param array $classDoc
     */
    public function request(&$classDoc)
    {

    }

    /**
     * returns a list of classes available to run on classes
     * @param $helpers
     */
    public function map(&$helpers)
    {
        $helpers[ Request::class ][] = HelpersRequest::class;
        $helpers[ Store::class ][] = HelpersStore::class;
        $helpers[ devPlusGeneratorAbstract::class ][] = GeneratorAbstract::class;
        $helpers[ Output::class ][] = \IPS\dtproxy\Helpers\Output::class;
        $helpers[ Model::class ][] = \IPS\dtproxy\Helpers\Model::class;
    }
}
