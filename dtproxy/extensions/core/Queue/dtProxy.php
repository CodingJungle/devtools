<?php
/**
 * @brief            Background Task
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @license          https://www.invisioncommunity.com/legal/standards/
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Proxy Class Generator
 * @since            22 Apr 2018
 */

namespace IPS\dtproxy\extensions\core\Queue;
use function defined;
use function header;
use function json_decode;
use function file_get_contents;

/* To prevent PHP errors (extending class does not exist) revealing path */
use IPS\Data\Store;
use IPS\dtproxy\Proxyclass;
use IPS\Task\Queue\OutOfRangeException;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * Background Task
 */
class _dtProxy
{
    /**
     * Parse data before queuing
     *
     * @param    array $data
     * @return    array
     */
    public function preQueueData($data)
    {
        return $data;
    }

    /**
     * Run Background Task
     *
     * @param    mixed $data   Data as it was passed to \IPS\Task::queue()
     * @param    int   $offset Offset
     * @return    int                            New offset
     * @throws    OutOfRangeException    Indicates offset doesn't exist and thus task is complete
     */
    public function run($data, $offset)
    {
        switch ($offset) {
            default:
                $files = Proxyclass::i()->dirIterator(\null, \true);
                foreach ($files as $file) {
                    Proxyclass::i()->build($file->getRealPath());
                }
                Store::i()->dtproxy_templates = Proxyclass::i()->templates;
                $offset = 1;
                break;
            case 1:
                \IPS\dtproxy\Generator\Proxy::i()->buildConstants();
                \IPS\dtproxy\Generator\Proxy::i()->generateSettings();
                if (Proxyclass::i()->doProxies) {
                    $offset = 2;
                } else {
                    throw new OutOfRangeException;
                }
                break;
            case 2:
                $path = \IPS\ROOT_PATH . '/applications/dtproxy/data/defaults/';
                $jsonMeta = json_decode(file_get_contents($path . 'defaults.json'), \true);
                $jsonMeta2 = json_decode(file_get_contents($path . 'defaults2.json'), \true);
                $jsonMeta += $jsonMeta2;
                Store::i()->dt_json = $jsonMeta;
                \IPS\dtproxy\Generator\Applications::i()->create();
                $offset = 3;
                break;
            case 3:
                \IPS\dtproxy\Generator\Db::i()->create();
                $offset = 4;
                break;
            case 4:
                \IPS\dtproxy\Generator\Language::i()->create();
                $offset = 5;
                break;
            case 5:
                \IPS\dtproxy\Generator\Extensions::i()->create();
                $offset = 6;
                break;
            case 6:
                \IPS\dtproxy\Generator\Templates::i()->create();
                $offset = 7;
                break;
            case 7:
                \IPS\dtproxy\Generator\Moderators::i()->create();
                $offset = 8;
                break;
            case 8:
                \IPS\dtproxy\Generator\Url::i()->create();
                $offset = 9;
                break;
            case 9:
                Proxyclass::i()->makeJsonFile();
                $offset = 10;
                break;
            case 10:
                unset(Store::i()->dtproxy_proxy_files, Store::i()->dtproxy_templates);
                throw new OutOfRangeException;
                break;
        }
        return $offset;
    }

    /**
     * Get Progress
     *
     * @param    mixed $data   Data as it was passed to \IPS\Task::queue()
     * @param    int   $offset Offset
     * @return    array( 'text' => 'Doing something...', 'complete' => 50 )    Text explaining task and percentage
     *                         complete
     * @throws    \OutOfRangeException    Indicates offset doesn't exist and thus task is complete
     */
    public function getProgress($data, $offset)
    {
        if ($offset === 10) {
            throw new \OutOfRangeException;
        }

        switch ($offset) {
            default:
                $text = 'Generating Proxy Classes';
                break;
            case 1:
                $text = 'Generating Constants and Settings';
                break;
            case 2 :
                $text = 'Generating App List';
                break;
            case 3:
                $text = 'Generating DB List';
                break;
            case 4:
                $text = 'Generating Language List';
                break;
            case 5:
                $text = 'Generating Extensions List';
                break;
            case 6:
                $text = 'Generating Templates List';
                break;
            case 7:
                $text = 'Generating Moderator Perms List';
                break;
            case 8:
                $text = 'Generating URL/Furl List';
                break;
            case 9:
                $text = 'Creating PHP-Toolbox meta json';
                break;
        }
        $total = 1;
        if (Proxyclass::i()->doProxies) {
            $total = 9;
        }
        $progress = ($offset / $total) * 100;

        return ['text' => $text, 'complete' => $progress];
    }

    /**
     * Perform post-completion processing
     *
     * @param    array $data
     * @return    void
     */
    public function postComplete($data)
    {
    }
}
