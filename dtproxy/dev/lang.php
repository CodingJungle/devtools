<?php

$lang = [

    '__app_dtproxy' => 'Dev Toolbox: Proxy Class Generator',
    'menu__dtproxy_proxy' => 'Proxy Class Generator',
    'menu__dtproxy_proxy_proxy' => 'Generator',
    'dtproxy_proxyclass_button' => 'Start',
    'dtproxy_sidebar' => 'Generates proxy classes for IPS/IPS Apps/3rd party apps, This is used for IDE\'s, so useful features such as autocomplete and hinting can be used, since IPS\'s framework is unique.',
    'dtproxy_proxyclass_title' => 'Proxy Classes',
    'dtproxy_done' => 'Proxy Classes have been Generated',
    'dtproxy_do_props' => 'Proxy Properties',
    'dtproxy_do_props_desc' => 'Adds @property/-read/-write to proxy classes, from the helper files/extensions/db tables.',
    'dtproxy_do_constants' => 'Proxy Constants',
    'dtproxy_do_constants_desc' => "The constants in IPS are defined in array, that get added dynamically in the bootstrapping of the framework, this makes it so most IDE's can not see them, this will build a proxy to them, so they wont appear as undefined in your code.",
    'dtproxy_do_props_doc' => 'Proxy Property DocBlock',
    'dtproxy_do_props_doc_desc' => 'Will create docblocks from the column definition for proxy props if enabled.',
    'dtproxy_progress' => 'Processing %s of %s',
    'dtproxy_do_proxies' => 'PHP-Toolbox Metadata',
    'dtproxy_do_proxies_desc' => 'if you have the <a target="_blank" href="https://plugins.jetbrains.com/plugin/8133-php-toolbox">PHP-Toolbox</a> plugin installed, this will generated appropriate files to be able to use for various methods.',
    'dtproxy_progress_extra' => 'Step Complete: %s, processing %s of %s',
    'dtproxy_tab' => 'Proxy Class Generator',
];
