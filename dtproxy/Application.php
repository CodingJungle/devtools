<?php
/**
 * @brief            Dev Tools: Proxy Class Generator Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Tools: Proxy Class Generator
 * @since            23 Mar 2018
 * @version          -storm_version-
 */

namespace IPS\dtproxy;

/**
 * Dev Tools: Proxy Class Generator Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
