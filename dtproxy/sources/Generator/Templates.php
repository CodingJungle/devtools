<?php
/**
 * @brief      Templates Singleton
 * @copyright  -storm_copyright-
 * @package    IPS Social Suite
 * @subpackage dtproxy
 * @since      -storm_since_version-
 * @version    -storm_version-
 */


namespace IPS\dtproxy\Generator;
use function defined;
use function header;
use function explode;
use function str_replace;

use Exception;
use IPS\Data\Store;
use IPS\Theme;
use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\FileGenerator;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\ParameterGenerator;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * Templates Class
 *
 * @mixin \IPS\dtproxy\Generator\Templates
 */
class _Templates extends GeneratorAbstract
{

    /**
     * @brief Singleton Instances
     * @note  This needs to be declared in any child class.
     * @var static
     */
    protected static $instance;

    /**
     * creates the jsonMeta for the json file and writes the provider to disk.
     */
    public function create()
    {
        $jsonMeta = [];
        if (isset(Store::i()->dt_json)) {
            $jsonMeta = Store::i()->dt_json;
        }
        $jsonMeta[ 'registrar' ][] = [
            'signature' => [
                "IPS\\Theme::getTemplate:0",
            ],
            'provider' => 'templateGroup',
            'language' => 'php',
        ];
        $jsonMeta[ 'registrar' ][] = [
            'signature' => [
                "IPS\\Theme::getTemplate:2",
                'IPS\\Output::js:2',
                'IPS\\Output::css:2',
            ],
            'provider' => 'templateLocation',
            'language' => 'php',
        ];
        $jsonMeta[ 'providers' ][] = [
            'name' => 'templateLocation',
            'lookup_strings' => [
                'admin',
                'front',
                'global',
            ],
        ];
        $jsonMeta[ 'registrar' ][] = [
            'signature' => [
                'IPS\\Theme::getTemplate:0',
            ],
            'signatures' => [
                [
                    'class' => Theme::class,
                    'method' => 'getTemplate',
                    'index' => 0,
                    'type' => 'type',
                ],

            ],
            'provider' => 'templateClass',
            'language' => 'php',
        ];
        $templates = [];
        $tempStore = [];
        $tempClass = [];

        if (isset(Store::i()->dtproxy_templates)) {
            $templates = Store::i()->dtproxy_templates;
        }
        if (\count($templates)) {
            foreach ($templates as $key => $template) {
                $temp = \explode(\DIRECTORY_SEPARATOR, $key);
                \array_pop($temp);
                $temp = \array_pop($temp);
                $ori = $temp;

                if ($temp === 'global') {
                    $temp = 'nglobal';
                }

                $tempStore[ $ori ] = [
                    'lookup_string' => $ori,
                    'type' => 'dtProxy\\Templates\\' . $temp,
                ];

                try {
                    $newParams = [];
                    if (!empty($template[ 'params' ])) {
                        $i = 0;
                        $params = explode(',', $template[ 'params' ]);
                        foreach ($params as $param) {
                            $prop = \null;
                            $propVal = explode('=', $param);
                            $type = \null;
                            if (isset($propVal[ 0 ])) {
                                $prop = str_replace('$', '', $propVal[ 0 ]);
                                $propType = explode(' ', $prop);
                                if (isset($propType[ 1 ])) {
                                    $prop = $propType[ 1 ];
                                    if (isset($propType[ 0 ])) {
                                        $type = $propType[ 0 ];
                                    }
                                }

                                if (isset($propType[ 0 ])) {
                                    $prop = $propType[ 0 ];
                                }
                            }

                            $value = \null;
                            if (isset($propVal[ 1 ])) {
                                $value = $propVal[ 1 ];
                            }
                            $newParams[] = new ParameterGenerator($prop, $type, $value, $i);
                        }
                    }
                    $tempClass[ $temp ][ $template[ 'method' ] ] = MethodGenerator::fromArray([
                        'name' => $template[ 'method' ],
                        'parameters' => $newParams,
                        'static' => \false,
                    ]);
                } catch (Exception $e) {
                }
            }
        }

        \ksort($tempStore);
        $tempStore = \array_values($tempStore);
        $jsonMeta[ 'providers' ][] = [
            'name' => 'templateClass',
            'items' => $tempStore,
        ];
        Store::i()->dt_json = $jsonMeta;
        $this->makeTempClasses($tempClass);
    }

    /**
     * @param array $classes
     */
    public function makeTempClasses(array $classes)
    {
        foreach ($classes as $key => $templates) {
            //            try {
            $newClass = new ClassGenerator;
            $newClass->setNamespaceName('dtProxy\Templates');
            $newClass->setName($key);
            $newClass->addMethods($templates);
            $content = new FileGenerator;
            $content->setClass($newClass);
            $content->setFilename($this->save . '/templates/' . $key . '.php');
            $content->write();
            //            }
            //            catch(Exception $e){}
        }
    }
}

