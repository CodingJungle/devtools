<?php

/**
 * @brief       Dtbase Menu extension: Menu
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Dev Toolbox: Content Generator
 * @since       1.0.3
 * @version     -storm_version-
 */

namespace IPS\dtcontent\extensions\dtbase\Menu;

use IPS\Http\Url;
use function defined;
use function header;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER[ 'SERVER_PROTOCOL' ]) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * menu
 */
class _menu
{

    /**
     * return an array for the menu
     */
    public function menu(&$menus)
    {
        if (!isset($menus[ 'roots' ][ 'dtbase' ])) {
            $menus[ 'roots' ][ 'dtbase' ] = [
                'id' => 'dtbase',
                'name' => 'Dev Toolbox',
                'url' => 'elDevToolsDTbase',
            ];
        }

        $menus[ 'dtbase' ][] = [
            'id' => 'content',
            'name' => 'Content Generator',
            'url' => (string)Url::internal('app=dtcontent&module=view&controller=generator'),
        ];
    }
}
