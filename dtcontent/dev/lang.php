<?php

$lang = [
    '__app_dtcontent' => 'Dev Toolbox: Content Generator',
    'menu__dtcontent_view' => 'Content Generator',
    'menu__dtcontent_view_view' => 'Generator',
    'dtcontent_type' => 'Content Type',
    'dtcontent_limit' => 'Limit',
    'dtcontent_passwords' => 'Random Passwords',
    'dtcontent_passwords_desc' => 'if enabled, passwords will be randomize, otherwise username is used.',
    'dtcontent_group' => 'Group',
    'dtcontent_group_desc' => 'Select the group you wish to make the members for.',
    'dtcontent_club' => 'Randomly join available clubs',
    'dtcontent_progress' => '%s out of %s',
    'dtcontent_completed' => '%s creation completed',
];
