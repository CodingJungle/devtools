<?php
/**
 * @brief            Dev Toolbox: Content Generator Application Class
 * @author           -storm_author-
 * @copyright        -storm_copyright-
 * @package          Invision Community
 * @subpackage       Dev Toolbox: Content Generator
 * @since            04 Apr 2018
 * @version          -storm_version-
 */

namespace IPS\dtcontent;

/**
 * Dev Toolbox: Content Generator Application Class
 */
class _Application extends \IPS\Application
{
    /**
     * @inheritdoc
     */
    protected function get__icon()
    {
        return 'wrench';
    }
}
